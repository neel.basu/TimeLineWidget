#include "timelinewidget.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    TimeLineWidget w;
    w.show();

    w.add(std::make_pair(20.0f, 80.0f));
    w.add(std::make_pair(90.0f, 110.0f));
    w.add(std::make_pair(120.0f, 180.0f));

    return a.exec();
}
