#include "timelinewidget.h"
#include "ui_timelinewidget.h"
#include <QDebug>

TimeLineWidget::TimeLineWidget(QWidget *parent): QWidget(parent), ui(new Ui::TimeLineWidget), _scene(new QGraphicsScene),ribbon_margin(10.0f), ribbon_height(0.2){
    ui->setupUi(this);
    ui->view->setScene(_scene);
    init();
    draw(size());
}

TimeLineWidget::~TimeLineWidget(){
    delete ui;
}

void TimeLineWidget::init(){
    _min = UINT_MAX;
    _max = 0.0f;

    _top_rule = new QGraphicsLineItem;
    _bottom_rule = new QGraphicsLineItem;
    _scene->addItem(_top_rule);
    _scene->addItem(_bottom_rule);
}

void TimeLineWidget::draw(const QSizeF& sz){
    double top    = _size.height()-_size.height()*ribbon_height;
    double bottom = _size.height()-ribbon_margin;

    QLineF marginb(QPointF(0.0f, bottom), QPointF(_size.width(), bottom));
    QLineF margint(QPointF(0.0f, top), QPointF(_size.width(), top));

    _top_rule->setLine(margint);
    _bottom_rule->setLine(marginb);

    //{ rescale existing rectangles
    for(QGraphicsRectItem* rect: _rects){
        double left   = rescale(0.0f, _size.width(), 0.0f, sz.width(), rect->rect().left());
        double right  = rescale(0.0f, _size.width(), 0.0f, sz.width(), rect->rect().right());
        rect->setRect(QRectF(QPointF(left, top), QPointF(right, bottom)));
    }
    //}

    _size = sz;
}

void TimeLineWidget::add(std::pair<quint64, quint64> range){
    double top    = _size.height()-_size.height()*ribbon_height;
    double bottom = _size.height()-ribbon_margin;
    double nmin = std::min((double)range.first, _min);
    double nmax = std::max((double)range.second, _max);

    //{ rescale existing rectangles
    for(QGraphicsRectItem* rect: _rects){
        double tl     = rescale(0.0f, _size.width(), nmin, _max, rect->rect().left());
        double left   = rescale(nmin, nmax, 0.0f, _size.width(), tl);
        double tr     = rescale(0.0f, _size.width(), nmin, _max, rect->rect().right());
        double right  = rescale(nmin, nmax, 0.0f, _size.width(), tr);
        rect->setRect(QRectF(QPointF(left, top), QPointF(right, bottom)));
    }
    //}

    _min = nmin;
    _max = nmax;

    double left   = rescale(_min, _max, 0.0f, _size.width(), range.first);
    double right  = rescale(_min, _max, 0.0f, _size.width(), range.second);

    QGraphicsRectItem* rect = _scene->addRect(QRectF(QPointF(left, top), QPointF(right, bottom)), QPen(), QBrush(Qt::red, Qt::SolidPattern));
    _rects << rect;
}

void TimeLineWidget::resizeEvent(QResizeEvent* ev){
    ui->view->setSceneRect(QRectF(QPointF(0.0f, 0.0f), _size));
    draw(ev->size());
}

double TimeLineWidget::rescale(double a, double b, double c, double d, double x){
    return -(-b*c + a*d)/(-a+b)+((-c+d)*x)/(-a+b);
}
