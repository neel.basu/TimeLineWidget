#ifndef TIMELINEWIDGET_H
#define TIMELINEWIDGET_H

#include <QWidget>
#include <QVector>
#include <QGraphicsScene>
#include <QResizeEvent>
#include <QGraphicsLineItem>
#include <QGraphicsRectItem>

namespace Ui {
class TimeLineWidget;
}

class TimeLineWidget : public QWidget{
  Q_OBJECT
  public:
    struct Region{
        quint64 start; // epoch value
        quint64 end;   // epoch value
        QGraphicsRectItem* rect;
    };

  private:
    double _min, _max;
    double ribbon_margin;
    double ribbon_height;
    QGraphicsScene* _scene;
    QSizeF _size;
    QGraphicsLineItem* _bottom_rule;
    QGraphicsLineItem* _top_rule;
    QVector<QGraphicsRectItem*> _rects;
  public:
    explicit TimeLineWidget(QWidget *parent = 0);
    ~TimeLineWidget();
  private:
    void init();
    void draw(const QSizeF &sz);
  public:
    void add(std::pair<quint64, quint64> range);
  private:
    void resizeEvent(QResizeEvent* ev);
    double rescale(double a, double b, double c, double d, double x);
  private:
    Ui::TimeLineWidget *ui;
};

#endif // TIMELINEWIDGET_H
